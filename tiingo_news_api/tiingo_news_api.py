"""Main module."""
import requests
from loguru import logger

from typing import Dict


class TiingoNewsAPI:
    """Returns a list containing :class:`bluepy.btle.Characteristic`
    objects for the peripheral. If no arguments are given, will return all
    characteristics. If startHnd and/or endHnd are given, the list is
    restricted to characteristics whose handles are within the given range.
    """

    def __init__(self, api_token: str):
        """
        :param api_token: Tiingo api endpoint to hit
        :type api_token: str
        """
        self.headers = {"Content-Type": "application/json"}

        self.base_url = "https://api.tiingo.com/tiingo"
        self.payload = {"token": api_token}

    def get_tiingo_data(self, api_endpoint: str, **kwargs) -> Dict:
        """Written to encapsulate the tiingo retrieval logic.

        :param api_endpoint: Tiingo api endpoint to hit
        :type api_endpoint: str
        :param kwargs: Kwargs
        :type kwargs: Keyword args, optional
        :return: Dictionary containing requested data
        :rtype: Dict
        """
        params = {**self.payload, **kwargs}
        response = requests.get(api_endpoint, headers=self.headers, params=params)

        if response.status_code == 200:
            logger.info("Request successful")
            return response.json()
        else:
            logger.error("Request error")
            logger.error(response.status_code)
            raise ConnectionError(response)

    def get_news(self, **kwargs) -> Dict:
        """
        :param kwargs: Kwargs
        :type kwargs: Keyword args, optional
        """
        api_endpoint = f"{self.base_url}/news"
        data = self.get_tiingo_data(api_endpoint, **kwargs)
        return data

    def get_eod_metadata(self, ticker: str) -> Dict:
        """Do things

        :param ticker: Stock ticker to retrieve
        :type ticker: str
        :return: Dictionary containing data
        :rtype: Dict
        """
        api_endpoint = f"{self.base_url}/daily/{ticker}"
        data = self.get_tiingo_data(api_endpoint)
        return data

    def get_eod_latest_price(self, ticker: str) -> Dict:
        """

        :param ticker: Stock ticker to retrieve
        :type ticker: str
        :return: Dictionary containing data
        :rtype: Dict
        """
        api_endpoint = f"{self.base_url}/daily/{ticker}/prices"
        data = self.get_tiingo_data(api_endpoint)
        return data

    def get_crypto(self, endpoint: str = "", **kwargs) -> Dict:
        """

        :param endpoint: Defines endpoint to hit, if left blank will hit base endpoint, other current available\
        endpoints are `prices`/`top`. Top returns top of book data, prices returns latest price.
        :type endpoint: str
        :param kwargs: Kwargs
        :type kwargs: Keyword args, optional
        :return: Dictionary containing data
        :rtype: Dict
        """
        if endpoint != "":
            api_endpoint = f"{self.base_url}/crypto/{endpoint}"
        else:
            api_endpoint = f"{self.base_url}/crypto/"
        data = self.get_tiingo_data(api_endpoint, **kwargs)
        return data

    def get_iex(self, ticker: str = "", **kwargs) -> Dict:
        """
        Retrieves iex data from Tiingo

        :param ticker: Stock ticker to retreive
        :type ticker: str
        :param kwargs: Kwargs
        :type kwargs: Keyword args, optional
        :return: Dictionary containing data
        :rtype: Dict
        """
        if ticker != "":
            api_endpoint = f"{self.base_url}/iex/{ticker}"
        else:
            api_endpoint = f"{self.base_url}/iex/"
        data = self.get_tiingo_data(api_endpoint, **kwargs)
        return data
