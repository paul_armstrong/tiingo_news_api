"""Top-level package for Tiingo News Api."""

__author__ = """Paul Armstrong"""
__email__ = "parmstrong@gitlab.com"
__version__ = "0.1.3"

from tiingo_news_api.tiingo_news_api import TiingoNewsAPI
