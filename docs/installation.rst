.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Tiingo News Api, run this command in your terminal:

.. code-block:: console

    $ pip install tiingo_news_api

This is the preferred method to install Tiingo News Api, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Tiingo News Api can be downloaded from the `gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/paul_armstrong/tiingo_news_api

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/paul_armstrong/tiingo_news_api/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _gitlab repo: https://gitlab.com/paul_armstrong/tiingo_news_api
.. _tarball: https://gitlab.com/paul_armstrong/tiingo_news_api/tarball/master
