===============
Tiingo News Api
===============


.. image:: https://gitlab.com/paul_armstrong/tiingo_news_api/badges/main/pipeline.svg
        :target: https://gitlab.com/paul_armstrong/tiingo_news_api/-/commits/main
        :alt: Pipeline Status

.. image:: https://gitlab.com/paul_armstrong/tiingo_news_api/badges/main/coverage.svg
        :target: https://gitlab.com/paul_armstrong/tiingo_news_api/-/commits/main
        :alt: Coverage report

.. image:: https://gitlab.com/paul_armstrong/tiingo_news_api/-/badges/release.svg
        :target: https://gitlab.com/paul_armstrong/tiingo_news_api/-/releases
        :alt: Latest Release

.. image:: https://gitlab.com/paul_armstrong/tiingo_news_api/-/badges/main/pipeline.svg
        :target: https://paul_armstrong.gitlab.io/tiingo_news_api
        :alt: Documentation



Simple python API wrapper for interacting with Tiingo endpoints


* Free software: MIT license
* Documentation: https://tiingo-news-api.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://gitlab.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://gitlab.com/audreyr/cookiecutter-pypackage

